# sicp-solutions

Solutions to the problems from the Structure and Interpretation of Computer Programs book using Racket language. 

## Why Racket?

I picked it up during picture language chapter because Racket has some built-in functions for drawing. I also used hash tables, letrec and let* which I couldn't find in my R5RS Scheme. Also I had to use mutable list structure where mutation was needed, which results sometimes in ugly list->mlist casts. Other than that the code should be legitimate Scheme.


## Note on the structure

The goal of these solutions is to provide immediately runnable code, which is sometimes achieved in cost of readablity. 

Exercises are grouped in modules, where they depend on a provided code or on each other. Chapter 4 solutions use imports and redefine some of the imported functions at the top of their files.

Since everything seems to depend on everything in chapter 5, the code for the most part is located in one huge file. I'm sorry for that.

Some exercises that didn't require coding were omitted, and some that I couldn't manage to solve in reasonable time were skipped.
