"""
Find coefficients for number of pushes needed to
compute fibonacci number in terms of next fibonacci number
"""
def find_coeffs(data):
    """given list of number of pushes find coefficients
    data[n] is number of pushes needed to perform to compute fib(n+2)"""
    for x in xrange(200):
        res = check(data, x)
        if res:
            return res

def get_diff(lst, k, idx):
    return lst[idx]-fibs[idx+1]*k

def check(lst, k):
    diff = get_diff(lst, k, 0)
    for n in xrange(1, len(lst)-1):
        new_diff = get_diff(lst, k, n)
        if diff != new_diff:
            return None
    return k, diff

def fib(n):
    if n < 2:
        return n
    else:
        return fib(n-1) + fib(n-2)
fibs = [fib(n) for n in range(2, 20)]

def print_result(impl_name, data):
    res = find_coeffs(data)
    if not res:
        print "No coefficients found for %s" % impl_name
    else:
        if res[1] > 0:
            offset_str = " + %d" % res[1]
        else:
            offset_str = " - %d" % abs(res[1])
        print "%s: %d*S(n+1)%s" % (impl_name, res[0], offset_str)

if __name__ == '__main__':
    compiled_data = [7, 10, 16, 25, 40, 64, 103, 166, 268]
    print_result("Compiled", compiled_data)
    special_data = [4, 8, 16, 28, 48, 80, 132, 216, 352]
    print_result("Special", special_data)
