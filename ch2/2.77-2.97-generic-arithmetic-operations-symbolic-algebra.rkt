#lang racket

(define (square x) (* x x))
(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (cond ((number? datum) 'scheme-number)
        ((pair? datum) (car datum))
        (else (error "Bad tagged datum:" datum))))

(define (contents datum)
(cond ((number? datum) datum)
      ((pair? datum) (cdr datum))
      (else (error "Bad tagged datum:" datum))))

(define *op-table* (make-hash))

(define (put op type proc)
  (hash-set! *op-table* (list op type) proc))

(define (get op type)
  (hash-ref *op-table* (list op type) #f))

;; find procedure appropriate to type tags of args
;; and apply it to args
(define (apply-generic op . args)
  (let ((proc (get-op-for-args op args)))
    (if proc
        (apply proc (map contents args))
        (let* ((coercions (find-all-coercions args))
               (result
                (ormap (lambda (coerc)
                         (let ((proc (get-op-for-args op coerc)))
                           (if proc
                               (apply proc (map contents coerc))
                               #f)))
                       coercions)))
          (if result
              result
              (error "NO METHOD FOUND -- APPLY GENERIC" (list op args)))))))

;; same as apply-generic, but trying to simplify resulting types
(define (apply-and-simplify op . args)
  (drop (apply apply-generic op args)))

;; try to get operation for given list of arguments with respect to their types
(define (get-op-for-args op args)
  (get op (map type-tag args)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Coercion with raise
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        
;; Try to coerce list of arguments to given type
;; if for some argument An of type Tn there is no
;; function Tn->type, return #f
(define (coerce-list type args)
  (let ((res
         (map
          (lambda (arg)
            (if (eq? (type-tag arg) type)
                arg
                (let ((fn (get-coercion (type-tag arg) type)))
                  (if fn
                      (fn arg)
                      #f))))
          args)))
    (if (not (member #f res))
        res
        #f)))

;; Find all possible coercions from args to every
;; type that is present in args
(define (find-all-coercions args)
  (filter (lambda (x) x)
          (map (lambda (arg)
                 (coerce-list (type-tag arg) args))
               args)))

;; #t if arg1 of type t1 can be successively raised into t2, type of arg2
(define (can-be-raised arg1 arg2)
  (let ((t1 (type-tag arg1))
        (t2 (type-tag arg2)))
    (cond ((eq? t1 t2)
           #t)
          ((get 'raise (list t1))
           (can-be-raised (raise arg1) arg2))
          (else #f))))

;; Find which type of two arguments is higher in type hierarchy and return it
;; #f if types are not in the same hierarchy
(define (cmp-types arg1 arg2)
  (cond ((can-be-raised arg1 arg2) (type-tag arg2))
        ((can-be-raised arg2 arg1) (type-tag arg1))
        (else #f)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Simplifying datatype with drop
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Simplify datatype of x by lowering its type in hierarchy as far as possible
(define (drop x)
  (if (can-project? x)
      (drop (project x))
      x))

;; True if datatype of x can be simplified without losing any information
(define (can-project? x)
  (and (pair? x)
       (get 'project (list (type-tag x)))
       (equ? x (raise (project x)))))

  
(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))
(define (reduce x y) (apply-generic 'reduce x y))
(define (equ? x y) (apply-generic 'equ? x y))
(define (=zero? x) (apply-generic '=zero? x))
(define (negate x) (apply-generic 'negate x))
(define (raise x) (apply-generic 'raise x))
(define (project x) (apply-generic 'project x))


;; Different number packages

;; Regular scheme numbers
(define (install-scheme-number-package)

  (define (reduce-integers n d)
    (let ((g (gcd n d)))
      (list (/ n g) (/ d g))))

  (put 'add '(scheme-number scheme-number)
       (lambda (x y) (+ x y)))
  (put 'sub '(scheme-number scheme-number)
       (lambda (x y) (- x y)))
  (put 'mul '(scheme-number scheme-number)
       (lambda (x y) (* x y)))
  (put 'div '(scheme-number scheme-number)
       (lambda (x y) (/ x y)))
  (put 'equ? '(scheme-number scheme-number)
       (lambda (x y) (= x y)))
  (put 'raise '(scheme-number)
       (lambda (x) (make-rational x 1)))
  (put '=zero? '(scheme-number)
       (lambda (x) (= x 0)))
  (put 'negate '(scheme-number)
       (lambda (x) (- x)))
  (put 'reduce '(scheme-number scheme-number)
       (lambda (x y) (reduce-integers x y)))
  'done)

(define (make-scheme-number x) x)

;; Rational numbers

(define (install-rational-package)
  ;; internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (make-rat n d)
    (let ((reduced (reduce n d)))
      (cons (car reduced) (cadr reduced))))
  (define (add-rat x y)
    (make-rat (add (mul (numer x) (denom y))
                   (mul (numer y) (denom x)))
              (mul (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (sub (mul (numer x) (denom y))
                   (mul (numer y) (denom x)))
              (mul (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (mul (numer x) (numer y))
              (mul (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (mul (numer x) (denom y))
              (mul (denom x) (numer y))))
    
  ;; interface to rest of the system
  (define (tag x) (attach-tag 'rational x))
  (put 'add '(rational rational)
       (lambda (x y) (tag (add-rat x y))))
  (put 'sub '(rational rational)
       (lambda (x y) (tag (sub-rat x y))))
  (put 'mul '(rational rational)
       (lambda (x y) (tag (mul-rat x y))))
  (put 'div '(rational rational)
       (lambda (x y) (tag (div-rat x y))))
  (put 'equ? '(rational rational)
       (lambda (x y)
         (and (= (numer x) (numer y))
              (= (denom x) (denom y)))))
  (put '=zero? '(rational)
       (lambda (x) (= (numer x) 0)))
  (put 'negate '(rational)
       (lambda (x) (tag (make-rat (- (numer x)) (denom x)))))
  (put 'make 'rational
       (lambda (n d) (tag (make-rat n d))))
  (put 'raise '(rational)
       (lambda (x) (make-real (/ (numer x) (denom x)))))
  (put 'project '(rational)
       (lambda (x) (round (/ (numer x) (denom x)))))
  'done)

(define (make-rational n d)
  ((get 'make 'rational) n d))

;; Real numbers

(define (install-real-package)
  ;; internal
  (define (tag x) (attach-tag 'real x))
  ;; interface
  (put 'make 'real
       (lambda (x) (tag x)))
  (put 'add '(real real)
       (lambda (x y) (tag (+ x y))))
  (put 'sub '(real real)
       (lambda (x y) (tag (- x y))))
  (put 'mul '(real real)
       (lambda (x y) (tag (* x y))))
  (put 'div '(real real)
       (lambda (x y) (tag (/ x y))))
  (put 'equ? '(real real)
       (lambda (x y) (= x y)))
  (put '=zero? '(real) (lambda (x) (= x 0)))
  (put 'negate '(real) (tag (lambda (x) (- x))))
  (put 'raise '(real)
       (lambda (x)
         (make-complex-from-real-imag x 0)))
  (put 'project '(real)
       (lambda (x) (make-rational (round x) 1)))
  'done)

(define (make-real x)
  ((get 'make 'real) x))
         

;; Complex numbers

(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (+ (square (real-part z))
             (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a) 
    (cons (* r (cos a)) (* r (sin a))))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part '(rectangular) real-part)
  (put 'imag-part '(rectangular) imag-part)
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-real-imag 'rectangular 
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular 
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-polar-package)
  ;; internal procedures
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z)
    (* (magnitude z) (cos (angle z))))
  (define (imag-part z)
    (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y) 
    (cons (sqrt (+ (square x) (square y)))
          (atan y x)))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'magnitude '(polar) magnitude)
  (put 'angle '(polar) angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar 
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(define (install-complex-package)
  ;; imported from rect and polar packages
  (install-rectangular-package)
  (install-polar-package)
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))
  ;; internal procedures
  (define (add-complex z1 z2)
    (make-from-real-imag (+ (real-part z1) (real-part z2))
                         (+ (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (- (real-part z1) (real-part z2))
                         (- (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (* (magnitude z1) (magnitude z2))
                       (+ (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
                       (- (angle z1) (angle z2))))
  ;; interface to rest of the system
  (define (tag z) (attach-tag 'complex z))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'make-from-real-imag 'complex 
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex
       (lambda (r a) (tag (make-from-mag-ang r a))))
  (put 'real-part '(complex) real-part)
  (put 'imag-part '(complex) imag-part)
  (put 'magnitude '(complex) magnitude)
  (put 'angle '(complex) angle)
  (put 'equ? '(complex complex)
       (lambda (z1 z2)
         (and (= (real-part z1) (real-part z2))
              (= (imag-part z1) (imag-part z2)))))
  (put '=zero? '(complex)
       (lambda (z) (and (= (real-part z) 0)
                        (= (imag-part z) 0))))
  (put 'negate '(complex)
       (lambda (z) (tag (make-from-real-imag (negate (real-part z))
                                             (negate (imag-part z))))))
  (put 'project '(complex)
       (lambda (z) (make-real (real-part z))))
  'done)

(define (real-part z) (apply-generic 'real-part z))
(define (imag-part z) (apply-generic 'imag-part z))
(define (magnitude z) (apply-generic 'magnitude z))
(define (angle z) (apply-generic 'angle z))
  
(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))

(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))

(install-scheme-number-package)
(install-rational-package)
(install-real-package)
(install-complex-package)

;; coercion
(define *coercion-table* (make-hash))

(define (put-coercion type1 type2 proc)
  (hash-set! *coercion-table* (list type1 type2) proc))

(define (get-coercion type1 type2)
  (hash-ref *coercion-table* (list type1 type2) #f))

(define (scheme-number->complex n)
  (make-complex-from-real-imag (contents n) 0))

(put-coercion 'scheme-number 'complex scheme-number->complex)

;; 2.77
;; By adding (put 'real-part '(complex) real-part) we reference real-part back, i.e. the body (apply-generic 'real-part z) is executed twice
;; First call rips off '(complex) tag, and dispatches second call of apply-generic. Second call rips off underlying tag: '(rectangular) or '(polar)
;; which looks up procedure defined in appropriate module and calls it on the argument.

;; 2.78
;; scheme-number package simplified, functions type-tag and contents are modified to work with standard number datatype

;; 2.79 add equ? to every package and to global scope
;(equ? (make-scheme-number 1) (make-scheme-number 2))
;(equ? (make-rational 1 2) (make-rational 2 4))
;(define z1 (make-complex-from-real-imag 5 4))
;(define z2 (make-complex-from-mag-ang (magnitude z1) (angle z1)))
;(define one (make-scheme-number 1))
;(equ? z1 z2)

;; 2.80 add =zero? to every package and to global scope
;(=zero? (make-scheme-number 0))
;(=zero? (make-scheme-number 2))
;(=zero? (make-rational 0 23))
;(=zero? (make-rational 1 23))
;(=zero? (make-complex-from-mag-ang 0 2)) 
;(=zero? (make-complex-from-real-imag 0 2))

;(add z1 one)

;; 2.81
;; a. If we add coercions from type a to the same type a, apply generic will go into infinite loop 
;; in the case procedure isn't present in the dispatch table. This will happen because apply-generic will call
;; itself with the same types over and over again
;; b. We don't really need coercions from type a to the same type. When we call apply-generic there could be two cases:
;;    1) There is a procedure in the dispatch table and we apply this procedure to arguments. No coercion needed.
;;    2) There is no procedure, but coercion to the same type won't help to find one, so we don't need coercion here either.


;; 2.82 apply-generic updated for arbitrary size. This is pretty ugly solution.
;; procedures coerce-list and find-all-coercions added

;; 2.83 raise operation added

;; 2.84 raise operation is used in list of types coercion (coerce-list)

;; 2.85 added project operation to every package except for the scheme-number
;; added procedure drop
;; added new procedure apply-and-simplify, that works exactly as apply-generic, but also tries to simplify the result
;; the problem with adding this feature to apply-generic is that some operations might produce errors when trying to drop result
;; the most obvious example would be raise operation. 
;(define zz (make-complex-from-real-imag 2 0))
;(add zz zz)


;; terms manipulation procedures
(define (make-term order coeff) (list order coeff))
(define (order term) (car term))
(define (coeff term) (cadr term))



(define (install-terms-package)
  ;; common functions
  (define (the-empty-termlist) '())
  (define (empty-terms? ts) (null? ts))
  ;; sparse
  (define (install-sparse-package)
    (define (first-term ts) (car ts))
    (define (rest-terms ts) (cdr ts))
    (define (adjoin-term t ts)
      (if (=zero? (coeff t))
          ts
          (cons t ts)))
    (define (negate-terms ts)
      (map (lambda (t) 
             (make-term (order t)
                        (negate (coeff t))))
           ts))

    (define (mul-by-number ts n)
      (map (lambda (t) 
             (make-term (order t)
                        (* n (coeff t))))
           ts))
             
    
    ;; interface to the system
    (define (tag ts) (attach-tag 'sparse-terms ts))
    (put 'make-empty-terms 'sparse-terms 
         (lambda () (tag (the-empty-termlist))))
    (put 'first-term '(sparse-terms)
         (lambda (ts) (first-term ts)))
    (put 'rest-terms '(sparse-terms)
         (lambda (ts) (tag (rest-terms ts))))
    (put 'empty-terms? '(sparse-terms)
         (lambda (ts) (empty-terms? ts)))
    ; I don't see other way since I don't have separate type for a single term
    (put 'make-adjoiner '(sparse-terms)
         (lambda (ts)
           (lambda (t) (tag (adjoin-term t ts)))))
    (put 'negate '(sparse-terms)
         (lambda (ts) (tag (negate-terms ts))))
    (put 'mul '(sparse-terms scheme-number)
         (lambda (ts n) (tag (mul-by-number ts n))))
    'done)
  
  ;; dense
  (define (install-dense-package)
    (define (first-term ts)
      (make-term (- (length ts) 1) (car ts)))
    (define (rest-terms ts) (cdr ts))
    (define (negate-terms ts) (map negate ts))
    (define (mul-by-number ts n)
      (map (lambda (t) (* t n)) ts))
    (define (adjoin-term t ts)
      (if (zero? (coeff t))
          ts
          (let ((o (order t))
                (l (- (length ts) 1)))
            (cond ((= o l) (cons (+ (coeff t) (car ts))
                                 (cdr ts)))
                  ((< o l)
                   (cons (car ts)
                         (adjoin-term t (cdr ts))))
                  (else (adjoin-term t (cons 0 ts)))))))
    ;; interface to the system
    (define (tag ts) (attach-tag 'dense-terms ts))
    (put 'make-empty-terms 'dense-terms 
         (lambda () (tag (the-empty-termlist))))
    (put 'first-term '(dense-terms)
         (lambda (ts) (first-term ts)))
    (put 'rest-terms '(dense-terms)
         (lambda (ts) (tag (rest-terms ts))))
    (put 'empty-terms? '(dense-terms)
         (lambda (ts) (empty-terms? ts)))
    (put 'make-adjoiner '(dense-terms)
         (lambda (ts)
           (lambda (t) (tag (adjoin-term t ts)))))
    (put 'negate '(dense-terms)
         (lambda (ts) (tag (negate-terms ts))))
    (put 'mul '(dense-terms scheme-number)
         (lambda (ts n) (tag (mul-by-number ts n))))
    'done)
  (install-sparse-package)
  (install-dense-package)
  'done)

(install-terms-package)


(define (make-empty-sparse-terms)
  ((get 'make-empty-terms 'sparse-terms)))
(define (make-empty-dense-terms)
  ((get 'make-empty-terms 'dense-terms)))
(define (first-term ts) (apply-generic 'first-term ts))
(define (rest-terms ts) (apply-generic 'rest-terms ts))
(define (empty-terms? ts) (apply-generic 'empty-terms? ts))
(define (adjoin-term t ts) ((apply-generic 'make-adjoiner ts) t))

;; Polynomial package
(define (install-polynomial-package)
  ;; internal procedures
  (define (make-poly var terms)
    (cons var terms))
  (define (var p) (car p))
  (define (terms p) (cdr p))
  (define (var? x) (symbol? x))
  (define (same-var? v1 v2)
    (and (var? v1) (var? v2) (eq? v1 v2)))

  (define (zero? p) (null? (terms p)))

  (define (negate-pol p)
    (make-poly (var p)
               (negate (terms p))))

  (define (add-terms ts1 ts2)
    (cond ((empty-terms? ts1) ts2)
          ((empty-terms? ts2) ts1)
          (else
           (let ((t1 (first-term ts1)) (t2 (first-term ts2)))
             (cond ((> (order t1) (order t2))
                    (adjoin-term
                     t1 (add-terms (rest-terms ts1) ts2)))
                   ((< (order t1) (order t2))
                    (adjoin-term
                     t2 (add-terms ts1 (rest-terms ts2))))
                   (else
                    (adjoin-term
                     (make-term (order t1)
                                (add (coeff t1) (coeff t2)))
                     (add-terms (rest-terms ts1)
                                (rest-terms ts2)))))))))

  (define (sub-terms ts1 ts2) (add-terms ts1 (negate ts2)))
  
  (define (mul-terms ts1 ts2)
    (if (empty-terms? ts1)
        (make-empty-sparse-terms)
        (add-terms (mult-term-by-all-terms (first-term ts1) ts2)
                   (mul-terms (rest-terms ts1) ts2))))

  (define (mult-term-by-all-terms t ts)
    (if (empty-terms? ts)
        (make-empty-sparse-terms)
        (let ((t2 (first-term ts)))
          (adjoin-term
           (make-term (+ (order t) (order t2))
                      (mul (coeff t) (coeff t2)))
           (mult-term-by-all-terms t (rest-terms ts))))))

  (define (div-terms ts1 ts2)
    (if (empty-terms? ts1)
        (list (make-empty-sparse-terms) (make-empty-sparse-terms))
        (let ((t1 (first-term ts1))
              (t2 (first-term ts2)))
          (if (> (order t2) (order t1))
              (list (make-empty-sparse-terms) ts1)
              (let* ((new-c (div (coeff t1) (coeff t2)))
                     (new-o (- (order t1) (order t2)))
                     (new-term (make-term new-o new-c))
                     (rest-of-result
                      (div-terms 
                       (sub-terms ts1 (mult-term-by-all-terms new-term ts2))
                       ts2)))
                (list (adjoin-term new-term (car rest-of-result)) ;add highest term to other terms in result
                      (cadr rest-of-result))))))) ;remainder

  (define (remainder-terms ts1 ts2)
    (cadr (div-terms ts1 ts2)))

  (define (pseudoremainder-terms ts1 ts2)
    (let* ((o1 (order (first-term ts1)))
           (o2 (order (first-term ts2)))
           (c (coeff (first-term ts2)))
           (factor (expt c (+ 1 (- o1 o2)))))
      (cadr (div-terms (mul ts1 factor) ts2))))

  (define (gcd-terms ts1 ts2)
    (if (empty-terms? ts2)
        (let* ((coeffs (map coeff (cdr ts1))) ;probably should've implemented coefficients selector for termlist
               (g (apply gcd coeffs))
               (gcd-term (make-term 0 g)))
          ; just divide ts1 by its gcd in an ugly way, since division is only defined for termlists
          (car (div-terms ts1 (adjoin-term gcd-term (make-empty-sparse-terms)))))
        (gcd-terms ts2 (pseudoremainder-terms ts1 ts2))))

  (define (reduce-terms ts1 ts2)
    (let ((gcdterms (gcd-terms ts1 ts2)))
      (list (car (div-terms ts1 gcdterms))
            (car (div-terms ts2 gcdterms)))))
  
  (define (add-poly p1 p2)
    (if (same-var? (var p1) (var p2))
        (make-poly (var p1)
                   (add-terms (terms p1)
                              (terms p2)))
        (error "Poly not in same var -- ADD POLY"
               (list p1 p2))))

  (define (sub-poly p1 p2)
    (add-poly p1 (negate-pol p2)))
  
  (define (mul-poly p1 p2)
    (if (same-var? (var p1) (var p2))
        (make-poly (var p1)
                   (mul-terms (terms p1)
                              (terms p2)))
        (error "Poly not in same var -- MUL POLY"
               (list p1 p2))))
  (define (div-poly p1 p2)
    (if (same-var? (var p1) (var p2))
        (map
         (lambda (terms) (tag (make-poly (var p1) terms)))
         (div-terms (terms p1) (terms p2)))
        (error "Poly not in same var -- DIV POLY"
               (list p1 p2))))
  
  ;throws away remainder
  (define (mod-poly p1 p2)
    (car (div-poly p1 p2)))

  (define (gcd-poly p1 p2)
    (if (same-var? (var p1) (var p2))
        (make-poly (var p1)
                   (gcd-terms (terms p1) (terms p2)))
        (error "Poly not in same var -- GCD POLY"
               (list p1 p2))))

  (define (reduce-poly p1 p2)
    (if (same-var? (var p1) (var p2))
        (let ((res (reduce-terms (terms p1) (terms p2))))
          (list (tag (make-poly (var p1) (car res)))
                (tag (make-poly (var p1) (cadr res)))))
        (error "Poly not in same var -- REDUCE POLY"
               (list p1 p2))))
  
  ;; interface to the rest of system
  (define (tag p) (attach-tag 'polynomial p))
  (put 'add '(polynomial polynomial)
       (lambda (p1 p2) (tag (add-poly p1 p2))))
  (put 'sub '(polynomial polynomial)
       (lambda (p1 p2) (tag (sub-poly p1 p2))))
  (put 'mul '(polynomial polynomial)
       (lambda (p1 p2) (tag (mul-poly p1 p2))))
  (put 'div '(polynomial polynomial)
       (lambda (p1 p2) (div-poly p1 p2)))
  (put '=zero? '(polynomial) zero?)
  (put 'negate '(polynomial) (lambda (p) (tag (negate-pol p))))
  (put 'make 'polynomial
       (lambda (var terms) (tag (make-poly var terms))))
  (put 'reduce '(polynomial polynomial)
       (lambda (p1 p2) (reduce-poly p1 p2)))
  'done)
(install-polynomial-package)

(define make-polynomial (get 'make 'polynomial))


;; 2.87 =zero? operation added to polynomial package
;; 2.88 negate general operation added, as well as sub operation to polynomial package
;; 2.89 - 2.90 terms package
;; 2.91 div-terms and div-poly procedures
;; 2.93 rational package updated
;; 2.94 added gcd-terms, gcd-poly and 'greatest-common-divisor general operation
;(define p1 (make-polynomial 'x '(sparse-terms (4 1) (3 -1) (2 -2) (1 2))))
;(define p2 (make-polynomial 'x '(sparse-terms (3 1) (1 -1))))
;; 2.96 added pseudoremainder-terms to polynomial package, as well as multiplication terms by a number
;; b. gcd-terms updated to reduce terms
;; 2.97 a.gcd-terms updated
;; b. added reduce operations. gcd operation by this point isn't used outside of packages and therefore is removed
;; from global scope

;(define p1 (make-polynomial 'x '(sparse-terms (2 1) (1 -2) (0 1))))
;(define p2 (make-polynomial 'x '(sparse-terms (2 11) (0 7))))
;(define p3 (make-polynomial 'x '(sparse-terms (1 13) (0 5))))
;(define q1 (mul p1 p2))
;(define q2 (mul p1 p3))
;(greatest-common-divisor q1 q2)