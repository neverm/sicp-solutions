#lang racket
(require scheme/mpair)

(define mcaar (compose mcar mcar))


;; 3.24 assoc procedure added to object and equal? procedure argument added to constructor
;; 3.25 update lookup and insert procedures to take a list of keys now, to support multidimensional tables.

(define (make-table equal?)
  (let ((local-table (mlist '*table*)))
    (define SINGLE '*SINGLEMARKER*)
    (define (assoc key records)
      (cond ((null? records) #f)
            ((equal? key (mcaar records)) (mcar records))
            (else (assoc key (mcdr records)))))

    (define (lookup table keys-list)
      (if (null? keys-list)
          #f
          (let ((subtable (assoc (car keys-list) (mcdr table))))
            (cond ((not subtable) #f)
                  ((null? (cdr keys-list)) (mcdr subtable))
                  (else (lookup subtable (cdr keys-list)))))))

    (define (insert table val keys-list)
      (if (null? keys-list)
          (error "Null keys list")
          (let ((subtable (assoc (car keys-list) (mcdr table))))
            (cond ((not subtable)
                   (if (eq? SINGLE (car keys-list))
                       ;insert pair (SINGLE, value)
                       (set-mcdr! table
                                  (mcons (mcons SINGLE val) (mcdr table)))
                       ;create missing subtable and insert in the same table again
                       (begin
                         (set-mcdr! table
                                  (mcons (mlist (car keys-list)) (mcdr table)))
                         (insert table val keys-list))))
                  (else
                   (if (eq? SINGLE (car keys-list))
                       ;subtable is now actually a node (SINGLE, oldval), reset it to the new val
                       (begin (set-mcdr! subtable val) 'ok)
                       (insert subtable val (cdr keys-list))))))))

    (define (dispatch m)
      (cond ((eq? m 'lookup)
             (lambda keys (lookup local-table
                                  (append keys (list SINGLE)))))
            ((eq? m 'insert)
             (lambda (val . keys)
               (insert local-table
                       val
                       (append keys (list SINGLE)))))
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

;(define t (make-table eq?))
;((t 'insert) 'a 1 1)
;((t 'insert) 'b 1 2)
;((t 'insert) 'b 1 1 1)
;((t 'insert) 'test-single 1)
;((t 'lookup) 1 2)
;((t 'lookup) 1 3)
;((t 'lookup) 1 1)
;((t 'lookup) 1 1 1)
;((t 'lookup) 1)

;; 2.26
(define (make-tree-table)
  (let ((tree (mlist)))
    (define (left tr) (mcar tr))
    (define (set-left! val tr) (set-mcar! tr val))
    (define (right tr) (mcar (mcdr (mcdr tr))))
    (define (set-right! val tr) (set-mcar! (mcdr (mcdr tr)) val))
    (define (get-node tr) (mcar (mcdr tr)))
    (define (assoc key tr)
      (if
       (null? tr)
       #f
       (let ((cur-key (mcar (get-node tr))))
         (cond ((eq? key cur-key) (get-node tr))
               ((< key cur-key) (assoc key (left tr)))
               (else (assoc key (right tr)))))))

    (define (lookup k)
      (let ((node (assoc k tree)))
        (if node
            (mcdr node)
            #f)))

    (define (insert k v tr)
      (if (null? tr)
          (mlist null (mcons k v) null)
          (let* ((node (get-node tr))
                 (cur-key (mcar node)))
            (cond ((< k cur-key)
                   (set-left! (insert k v (left tr)) tr))
                  ((> k cur-key)
                   (set-right! (insert k v (right tr)) tr))
                  (else
                   (set-mcdr! node v)))
            tr)))

    (define (dispatch m)
      (cond ((eq? m 'lookup) lookup)
            ((eq? m 'insert) (lambda (k v) (set! tree (insert k v tree))))
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

;(define t (make-tree-table))
;((t 'insert) 1 2)
;((t 'insert) 2 3)
;((t 'lookup) 2)

(define (memoize f)
  (let ((table (make-table eq?)))
    (lambda (x)
      (let ((previously-computed-result ((table 'lookup) x)))
        (or previously-computed-result
            (let ((result (f x)))
              ((table 'insert) result x)
              result))))))

(define (fib n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else (+ (fib (- n 1))
                 (fib (- n 2))))))

;; 3.27
;; This works even though it looks weird:
;; 1. memo-fib name is binded to an expression not a lambda expression and it's body is evaluated early
;; 2. After evaluation, this name is in the global environment, but it's binded to whatever memoize function returned
;; 3. Returned happens to be a funcion, that has access to the table and uses table for argument caching
;; 4. Since memo-fib name is binded to this fuction, when we recursively call memo-fib we call it again recursively
(define memo-fib
  (memoize (lambda (n)
             (cond ((= n 0) 0)
                   ((= n 1) 1)
                   (else (+ (memo-fib (- n 1))
                            (memo-fib (- n 2))))))))


;; This will not work, because fib will call itself recursively without any caching
;; the result will be cached though, so successive calls will be much faster
(define memo-fib2 (memoize fib))

